package ru.ace.rssreader;

/**
 * Application constants
 */
public class Constants {

    public interface SharedPrefs {
        String SHARED_PREFS_FILE_NAME = "prefPS";
        String RSS_HOST_KEY = "rssHost";
        String LAST_OPENED_RSS_ITEM_ID = "lastOpenedRssItemId";
    }

    public interface RssBitmap {
        int THUMBNAIL_HEIGHT = 96;
        int THUMBNAIL_WIDTH = 128;
    }
}
