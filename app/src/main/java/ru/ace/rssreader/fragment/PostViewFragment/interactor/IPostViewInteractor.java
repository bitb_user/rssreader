package ru.ace.rssreader.fragment.PostViewFragment.interactor;

import ru.ace.rssreader.model.RssItem;

public interface IPostViewInteractor {
    RssItem getRssItemById(String rssItemId);
}
