package ru.ace.rssreader.fragment.PostsListFragment.interactor;

import java.util.List;

import javax.inject.Inject;

import ru.ace.rssreader.listener.IRssDbLoadListener;
import ru.ace.rssreader.listener.IRssHostCheckListener;
import ru.ace.rssreader.listener.IRssOnlineLoadListener;
import ru.ace.rssreader.manager.IRssFeedManager;
import ru.ace.rssreader.manager.IRssHostManager;
import ru.ace.rssreader.model.RssItem;

/**
 * Posts list interactor implementation
 */
public class PostsListInteractorImpl implements IPostsListInteractor {

    @Inject
    IRssHostManager mRssHostManager;

    @Inject
    IRssFeedManager mRssFeedManager;

    @Inject
    PostsListInteractorImpl() {
    }

    /**
     * Checks saved rss host url
     *
     * @param rssHostCheckListener on host check finish listener
     */
    @Override
    public void checkSavedRssHost(IRssHostCheckListener rssHostCheckListener) {
        mRssHostManager.checkRssHost(mRssHostManager.getSavedRssHost(), rssHostCheckListener);
    }

    /**
     * Loads saved in db rss posts
     *
     * @param rssCacheLoadListener on saved rss data load finish listener
     */
    @Override
    public void loadRssCachedData(IRssDbLoadListener rssCacheLoadListener) {
        mRssFeedManager.loadCachedRssData(rssCacheLoadListener);
    }

    /**
     * Loads online rss data
     *
     * @param rssOnlineLoadListener on online rss data load finish listener
     */
    @Override
    public void loadRssOnlineData(IRssOnlineLoadListener rssOnlineLoadListener) {
        mRssFeedManager.loadOnlineRssData(rssOnlineLoadListener);
    }

    /**
     * Saves rss data to db
     *
     * @param rssItems rss items to save
     */
    @Override
    public void saveRssDataToLocalDb(List<RssItem> rssItems) {
        mRssFeedManager.saveRssDataToLocalDb(rssItems);
    }
}
