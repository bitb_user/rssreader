package ru.ace.rssreader.fragment.PostPreviewFragment.view;

import android.graphics.Bitmap;

/**
 * Post preview fragment view interface
 */
public interface IPostPreviewFragmentView {
    void setBitmap(Bitmap bitmap);

    void setTitle(String title);

    void setPublicationDate(String pubDate);

    void setAuthor(String author);

    void setDescription(String description);
}
