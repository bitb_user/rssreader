package ru.ace.rssreader.fragment.SettingsFragment.presenter;

import javax.inject.Inject;

import ru.ace.rssreader.R;
import ru.ace.rssreader.fragment.SettingsFragment.interactor.ISettingsFragmentInteractor;
import ru.ace.rssreader.fragment.SettingsFragment.view.ISettingsFragmentView;
import ru.ace.rssreader.listener.IRssHostCheckListener;
import ru.ace.rssreader.manager.RssHostManager;

/**
 * Settings fragment presenter implementation
 */
public class SettingsFragmentPresenterImpl implements ISettingsFragmentPresenter {

    private ISettingsFragmentView mSettingsFragmentView;
    private ISettingsFragmentInteractor mSettingsFragmentInteractor;

    @Inject
    SettingsFragmentPresenterImpl(ISettingsFragmentView settingsFragmentView,
                                  ISettingsFragmentInteractor settingsFragmentInteractor) {

        mSettingsFragmentView = settingsFragmentView;
        mSettingsFragmentInteractor = settingsFragmentInteractor;
    }

    /**
     * Loads saved rss host url from shared preferences
     */
    @Override
    public void loadSavedRssHost() {
        String hostUrl = mSettingsFragmentInteractor.getSavedRssHost();
        mSettingsFragmentView.setRssHostUrl(hostUrl);
    }


    /**
     * Saves rss host url to shared preferences
     *
     * @param hostUrl rss host url to save
     */
    @Override
    public void saveRssHost(String hostUrl) {
        mSettingsFragmentInteractor.saveRssHost(hostUrl);
    }

    /**
     * Checks rss host validity
     *
     * @param hostUrl rss host url to check
     */
    @Override
    public void checkRssHost(String hostUrl) {
        mSettingsFragmentInteractor.checkRssHost(hostUrl, new IRssHostCheckListener() {

            @Override
            public void onNetUnavailable() {
                mSettingsFragmentView.showSnackbarMessage(R.string.message_net_is_not_available);
            }

            @Override
            public void onCheckFinish(int hostStatus) {
                switch (hostStatus) {
                    case RssHostManager.HostStatus.VALID:
                        mSettingsFragmentView.showSnackbarMessage(R.string.message_valid_rss_host);
                        break;
                    case RssHostManager.HostStatus.INVALID:
                        mSettingsFragmentView.showSnackbarMessage(R.string.message_invalid_rss_host);
                        break;
                    case RssHostManager.HostStatus.NOT_SPECIFIED:
                        mSettingsFragmentView.showSnackbarMessage(R.string.message_rss_host_is_not_specified);
                        break;
                }
            }
        });
    }
}
