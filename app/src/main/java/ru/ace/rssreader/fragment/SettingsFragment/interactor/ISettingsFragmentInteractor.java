package ru.ace.rssreader.fragment.SettingsFragment.interactor;


import ru.ace.rssreader.listener.IRssHostCheckListener;

/**
 * Settings fragment interactor interface
 */
public interface ISettingsFragmentInteractor {

    String getSavedRssHost();

    void saveRssHost(String hostUrl);

    void checkRssHost(String hostUrl, IRssHostCheckListener listener);
}
