package ru.ace.rssreader.fragment.PostPreviewFragment.presenter;

/**
 * Post preview fragment presenter interface
 */
public interface IPostPreviewFragmentPresenter {
    void loadData();
}
