package ru.ace.rssreader.fragment.PostPreviewFragment.presenter;

import android.graphics.Bitmap;

import javax.inject.Inject;

import ru.ace.rssreader.Constants;
import ru.ace.rssreader.async.bitmap.BitmapDownloadAsyncTask;
import ru.ace.rssreader.util.IDateUtils;
import ru.ace.rssreader.fragment.PostPreviewFragment.interactor.IPostPreviewFragmentInteractor;
import ru.ace.rssreader.fragment.PostPreviewFragment.view.IPostPreviewFragmentView;
import ru.ace.rssreader.manager.ISharedPrefsManager;
import ru.ace.rssreader.model.RssItem;

/**
 * Post preview fragment presenter implementation
 */
public class PostPreviewFragmentPresenterImpl implements IPostPreviewFragmentPresenter {

    @Inject
    IDateUtils mDateUtils;

    @Inject
    ISharedPrefsManager mSharedPrefsManager;

    private IPostPreviewFragmentView mPostPreviewFragmentView;
    private IPostPreviewFragmentInteractor mPostPreviewFragmentInteractor;

    @Inject
    PostPreviewFragmentPresenterImpl(IPostPreviewFragmentView postPreviewFragmentView,
                                     IPostPreviewFragmentInteractor postPreviewFragmentInteractor) {

        mPostPreviewFragmentView = postPreviewFragmentView;
        mPostPreviewFragmentInteractor = postPreviewFragmentInteractor;
    }

    public void loadData() {
        String rssItemId = mSharedPrefsManager.getString(Constants.SharedPrefs.LAST_OPENED_RSS_ITEM_ID, "");
        RssItem rssItem = mPostPreviewFragmentInteractor.getRssItemById(rssItemId);

        String pubDate = mDateUtils.convertPubDateToUserFormat(rssItem.getPubDate());
        mPostPreviewFragmentView.setPublicationDate(pubDate);
        mPostPreviewFragmentView.setTitle(rssItem.getTitle());
        mPostPreviewFragmentView.setAuthor(rssItem.getAuthor());
        mPostPreviewFragmentView.setDescription(rssItem.getDescription());

        loadRssBitmap(rssItem.getBitmapUrl());
    }

    /**
     * Async loading of rss bitmap
     *
     * @param bitmapUrl rss bitmap url
     */
    private void loadRssBitmap(final String bitmapUrl) {
        if (bitmapUrl == null || bitmapUrl.isEmpty())
            return;

        new BitmapDownloadAsyncTask(bitmapUrl) {
            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null) {
                    mPostPreviewFragmentView.setBitmap(bitmap);
                }
            }
        }.execute();
    }
}
