package ru.ace.rssreader.fragment.SettingsFragment.view;

import android.support.annotation.StringRes;

/**
 * Settings fragment view interface
 */
public interface ISettingsFragmentView {

    void setRssHostUrl(String url);

    void showSnackbarMessage(@StringRes int stringId);
}
