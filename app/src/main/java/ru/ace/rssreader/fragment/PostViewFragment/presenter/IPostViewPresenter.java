package ru.ace.rssreader.fragment.PostViewFragment.presenter;

public interface IPostViewPresenter {

    void loadData();
}
