package ru.ace.rssreader.fragment.PostsListFragment.view;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.ace.rssreader.Constants;
import ru.ace.rssreader.R;
import ru.ace.rssreader.adapter.PostsListAdapter;
import ru.ace.rssreader.di.components.IPostsListFragmentComponent;
import ru.ace.rssreader.di.modules.PostsListFragmentModule;
import ru.ace.rssreader.fragment.BaseFragment;
import ru.ace.rssreader.fragment.PostsListFragment.presenter.IPostsListPresenter;
import ru.ace.rssreader.listener.OnSelectRssItemListener;
import ru.ace.rssreader.manager.ISharedPrefsManager;
import ru.ace.rssreader.model.RssItem;
import ru.ace.rssreader.router.IMainRouter;

/**
 * Posts list fragment
 */
public class PostsListFragment extends BaseFragment implements IPostsListView, SwipeRefreshLayout.OnRefreshListener, OnSelectRssItemListener {

    public static final String TAG = PostsListFragment.class.getSimpleName();

    @Inject
    IPostsListPresenter mPostsListPresenter;

    @Inject
    ISharedPrefsManager mSharedPrefsManager;

    @BindView(R.id.swipeRefreshLayout_refreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.recycleView_postsList)
    RecyclerView mRecyclerView;

    private PostsListAdapter mAdapter;

    /**
     * Dependency injection components initialization
     */
    public void initDiComponent() {
        IPostsListFragmentComponent.PlusComponent activityComponent = getActivityComponent();
        IPostsListFragmentComponent component = activityComponent.plusIPostsListFragmentComponent(new PostsListFragmentModule(PostsListFragment.this));
        component.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_posts_list, container, false);
        ButterKnife.bind(this, root);
        initDiComponent();

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });

        mAdapter = new PostsListAdapter(new ArrayList<RssItem>(), mPostsListPresenter, this);
        mRecyclerView.setAdapter(mAdapter);

        mPostsListPresenter.loadRssData();
        return root;
    }

    /**
     * Calls on swipe refresh pulls down
     */
    @Override
    public void onRefresh() {
        mPostsListPresenter.loadOnlineRssDataIfAvailable();
    }

    @Override
    public void updateRssList(final List<RssItem> rssList) {
        mAdapter.updateData(rssList);
    }

    @Override
    public void showSwipeRefreshProgressBar() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideSwipeRefreshProgressBar() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showSnackbarMessage(@StringRes int stringId) {
        showSnackbarMessage(stringId, Snackbar.LENGTH_LONG);
    }

    /**
     * Go to PostPreviewFragment on list item clicked
     *
     * @param rssItemId clicked item guid
     */
    @Override
    public void onSelectRssItem(String rssItemId) {
        mSharedPrefsManager.setString(Constants.SharedPrefs.LAST_OPENED_RSS_ITEM_ID, rssItemId);
        ((IMainRouter) getActivity()).showPostPreviewFragment();
    }
}
