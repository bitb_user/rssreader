package ru.ace.rssreader.fragment.SettingsFragment.interactor;

import javax.inject.Inject;

import ru.ace.rssreader.Constants;
import ru.ace.rssreader.listener.IRssHostCheckListener;
import ru.ace.rssreader.manager.IRssHostManager;
import ru.ace.rssreader.manager.ISharedPrefsManager;

/**
 * Settings fragment interactor implementation
 */
public class SettingsFragmentInteractorImpl implements ISettingsFragmentInteractor {

    @Inject
    ISharedPrefsManager mSharedPrefsManager;

    @Inject
    IRssHostManager mRssHostManager;

    @Inject
    SettingsFragmentInteractorImpl() {
    }

    @Override
    public String getSavedRssHost() {
        return mSharedPrefsManager.getString(Constants.SharedPrefs.RSS_HOST_KEY, "");
    }

    @Override
    public void saveRssHost(String hostUrl) {
        mSharedPrefsManager.setString(Constants.SharedPrefs.RSS_HOST_KEY, hostUrl);
    }

    @Override
    public void checkRssHost(String hostUrl, IRssHostCheckListener listener) {
        mRssHostManager.checkRssHost(hostUrl, listener);
    }
}
