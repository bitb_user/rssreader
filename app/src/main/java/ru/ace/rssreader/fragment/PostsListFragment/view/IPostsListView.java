package ru.ace.rssreader.fragment.PostsListFragment.view;

import android.support.annotation.StringRes;

import java.util.List;

import ru.ace.rssreader.model.RssItem;

/**
 * Posts list view interface
 */
public interface IPostsListView {

    void showSwipeRefreshProgressBar();

    void hideSwipeRefreshProgressBar();

    void showSnackbarMessage(@StringRes int stringId);

    void updateRssList(List<RssItem> rssList);
}
