package ru.ace.rssreader.fragment.SettingsFragment.presenter;

/**
 * Settings fragment presenter interface
 */
public interface ISettingsFragmentPresenter {

    void loadSavedRssHost();

    void saveRssHost(String hostUrl);

    void checkRssHost(String hostUrl);
}
