package ru.ace.rssreader.fragment.SettingsFragment.view;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.ace.rssreader.R;
import ru.ace.rssreader.di.components.ISettingsFragmentComponent;
import ru.ace.rssreader.di.modules.SettingsFragmentModule;
import ru.ace.rssreader.fragment.BaseFragment;
import ru.ace.rssreader.fragment.SettingsFragment.presenter.ISettingsFragmentPresenter;

/**
 * Setting fragment
 */
public class SettingsFragment extends BaseFragment implements ISettingsFragmentView {

    public static final String TAG = SettingsFragment.class.getSimpleName();

    @Inject
    ISettingsFragmentPresenter mSettingsFragmentPresenter;

    @BindView(R.id.editText_hostUrl)
    EditText mHostUrlTextView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, root);
        initDiComponent();
        mSettingsFragmentPresenter.loadSavedRssHost();
        return root;
    }

    /**
     * Dependency injection components initialization
     */
    @Override
    public void initDiComponent() {
        ISettingsFragmentComponent.PlusComponent activityComponent = getActivityComponent();
        ISettingsFragmentComponent component = activityComponent.plusISettingsFragmentComponent(new SettingsFragmentModule(SettingsFragment.this));
        component.inject(this);
    }

    @Override
    public void setRssHostUrl(String hostUrl) {
        mHostUrlTextView.setText(hostUrl);
    }

    @Override
    public void showSnackbarMessage(@StringRes int stringId) {
        showSnackbarMessage(stringId, Snackbar.LENGTH_LONG);
    }

    @OnClick(R.id.button_save)
    public void onClick() {
        String hostUrl = mHostUrlTextView.getText().toString();
        mSettingsFragmentPresenter.saveRssHost(hostUrl);
        mSettingsFragmentPresenter.checkRssHost(hostUrl);
        closeSoftKeyboard();
    }
}
