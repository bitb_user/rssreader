package ru.ace.rssreader.fragment.PostViewFragment.view;

public interface IPostViewFragmentView {
    void loadUrl(String url);
}
