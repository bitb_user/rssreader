package ru.ace.rssreader.fragment.PostsListFragment.presenter;

import ru.ace.rssreader.listener.IRssBitmapLoadListener;

/**
 * Posts lists presenter interface
 */
public interface IPostsListPresenter extends IRssBitmapLoadListener {
    void loadRssData();
    void loadOnlineRssDataIfAvailable();
}
