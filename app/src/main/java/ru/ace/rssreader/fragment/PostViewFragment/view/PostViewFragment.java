package ru.ace.rssreader.fragment.PostViewFragment.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.ace.rssreader.R;
import ru.ace.rssreader.di.components.IPostViewFragmentComponent;
import ru.ace.rssreader.di.modules.PostViewFragmentModule;
import ru.ace.rssreader.fragment.BaseFragment;
import ru.ace.rssreader.fragment.PostViewFragment.presenter.IPostViewPresenter;

/**
 * Post view fragment
 * Shows original rss post via WebView
 */
public class PostViewFragment extends BaseFragment implements IPostViewFragmentView {

    public static final String TAG = PostViewFragment.class.getSimpleName();

    @Inject
    IPostViewPresenter mPostViewPresenter;

    @BindView(R.id.webView_post)
    WebView mPostView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_post_view, container, false);
        ButterKnife.bind(this, root);
        initDiComponent();

        //To avoid WebView to launch the default browser when open initial page
        mPostView.setWebViewClient(new WebViewClient());

        mPostViewPresenter.loadData();
        return root;
    }

    /**
     * Dependency injection components initialization
     */
    @Override
    public void initDiComponent() {
        IPostViewFragmentComponent.PlusComponent activityComponent = getActivityComponent();
        IPostViewFragmentComponent component = activityComponent.plusPostViewFragmentComponent(new PostViewFragmentModule(PostViewFragment.this));
        component.inject(this);
    }

    @Override
    public void loadUrl(String url){
        mPostView.loadUrl(url);
    }
}
