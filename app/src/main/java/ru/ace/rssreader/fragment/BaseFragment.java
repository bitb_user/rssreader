package ru.ace.rssreader.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

import ru.ace.rssreader.activity.IBaseActivity;
import ru.ace.rssreader.di.IHasComponent;

/**
 * Base fragment class
 */
public abstract class BaseFragment extends Fragment {

    public static final String TAG = BaseFragment.class.getSimpleName();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInsanceState) {
        super.onActivityCreated(savedInsanceState);
        initDiComponent();
    }

    abstract protected void initDiComponent();

    @SuppressWarnings("unchecked")
    public <T> T getActivityComponent() {
        Activity activity = getActivity();
        IHasComponent<T> has = (IHasComponent<T>) activity;
        return has.getComponent();
    }

    protected void closeSoftKeyboard() {
        ((IBaseActivity) getActivity()).closeSoftKeyboard();
    }

    protected void showSnackbarMessage(String message, int length) {
        ((IBaseActivity) getActivity()).showSnackbar(message, length);
    }

    protected void showSnackbarMessage(@StringRes int stringId, int length) {
        String message = getString(stringId);
        showSnackbarMessage(message, length);
    }
}
