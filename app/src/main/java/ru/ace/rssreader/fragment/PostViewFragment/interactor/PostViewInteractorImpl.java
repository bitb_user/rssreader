package ru.ace.rssreader.fragment.PostViewFragment.interactor;

import javax.inject.Inject;

import ru.ace.rssreader.manager.IDatabaseManager;
import ru.ace.rssreader.model.RssItem;

public class PostViewInteractorImpl implements IPostViewInteractor {

    @Inject
    PostViewInteractorImpl() {
    }

    @Inject
    IDatabaseManager mDatabaseManager;

    @Override
    public RssItem getRssItemById(String rssId) {
        return mDatabaseManager.getForId(rssId, RssItem.class);
    }
}
