package ru.ace.rssreader.fragment.PostViewFragment.presenter;

import javax.inject.Inject;

import ru.ace.rssreader.Constants;
import ru.ace.rssreader.fragment.PostViewFragment.interactor.IPostViewInteractor;
import ru.ace.rssreader.fragment.PostViewFragment.view.IPostViewFragmentView;
import ru.ace.rssreader.manager.ISharedPrefsManager;
import ru.ace.rssreader.model.RssItem;

public class PostViewPresenterImpl implements IPostViewPresenter {

    @Inject
    ISharedPrefsManager mSharedPrefsManager;

    private IPostViewFragmentView mPostViewFragmentView;
    private IPostViewInteractor mPostViewInteractor;

    @Inject
    PostViewPresenterImpl(IPostViewFragmentView postViewFragmentView,
                          IPostViewInteractor postViewInteractor) {

        mPostViewFragmentView = postViewFragmentView;
        mPostViewInteractor = postViewInteractor;
    }

    @Override
    public void loadData() {
        String rssId = mSharedPrefsManager.getString(Constants.SharedPrefs.LAST_OPENED_RSS_ITEM_ID, "");
        RssItem rssItem = null;

        if (!rssId.isEmpty())
            rssItem = mPostViewInteractor.getRssItemById(rssId);

        if (rssItem != null)
            mPostViewFragmentView.loadUrl(rssItem.getLink());
    }
}
