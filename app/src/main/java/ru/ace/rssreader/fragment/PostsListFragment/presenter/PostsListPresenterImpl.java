package ru.ace.rssreader.fragment.PostsListFragment.presenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ru.ace.rssreader.R;
import ru.ace.rssreader.util.IDateUtils;
import ru.ace.rssreader.fragment.PostsListFragment.interactor.IPostsListInteractor;
import ru.ace.rssreader.fragment.PostsListFragment.view.IPostsListView;
import ru.ace.rssreader.listener.IRssDbLoadListener;
import ru.ace.rssreader.listener.IRssHostCheckListener;
import ru.ace.rssreader.listener.IRssOnlineLoadListener;
import ru.ace.rssreader.manager.RssHostManager;
import ru.ace.rssreader.model.RssItem;

/**
 * Posts list presenter implementation
 */
public class PostsListPresenterImpl implements IPostsListPresenter {

    @Inject
    IDateUtils mDateUtils;

    private IPostsListView mPostsListView;
    private IPostsListInteractor mPostsListInteractor;

    private List<RssItem> mRssItems = new ArrayList<>();

    @Inject
    PostsListPresenterImpl(IPostsListView postsListView, IPostsListInteractor postsListInteractor) {
        mPostsListView = postsListView;
        mPostsListInteractor = postsListInteractor;
    }

    /**
     * Loads rss posts
     */
    @Override
    public void loadRssData() {
        mPostsListInteractor.loadRssCachedData(new IRssDbLoadListener() {

            @Override
            public void onSavedRssLoaded(List<RssItem> rssItems) {
                mRssItems.clear();
                addDataToRssList(rssItems);
                loadOnlineRssDataIfAvailable();
            }

            @Override
            public void onRssCacheLoadedFailed() {
                mPostsListView.hideSwipeRefreshProgressBar();
                mPostsListView.showSnackbarMessage(R.string.message_rss_loading_failed);
            }
        });
    }

    /**
     * Loads online rss posts if available
     */
    public void loadOnlineRssDataIfAvailable() {
        mPostsListInteractor.checkSavedRssHost(new IRssHostCheckListener() {

            @Override
            public void onNetUnavailable() {
                mPostsListView.updateRssList(mRssItems);
                mPostsListView.hideSwipeRefreshProgressBar();
                mPostsListView.showSnackbarMessage(R.string.message_net_is_not_available);
            }

            @Override
            public void onCheckFinish(int hostStatus) {
                switch (hostStatus) {
                    case RssHostManager.HostStatus.VALID:
                        loadOnlineRssData();
                        break;
                    case RssHostManager.HostStatus.INVALID:
                        mPostsListView.updateRssList(mRssItems);
                        mPostsListView.hideSwipeRefreshProgressBar();
                        mPostsListView.showSnackbarMessage(R.string.message_invalid_rss_host);
                        break;
                    case RssHostManager.HostStatus.NOT_SPECIFIED:
                        mPostsListView.updateRssList(mRssItems);
                        mPostsListView.hideSwipeRefreshProgressBar();
                        mPostsListView.showSnackbarMessage(R.string.message_rss_host_is_not_specified);
                        break;
                }
            }
        });
    }

    /**
     * Loads online rss posts
     */
    private void loadOnlineRssData() {
        mPostsListInteractor.loadRssOnlineData(new IRssOnlineLoadListener() {

            @Override
            public void onRssOnlineDataLoaded(List<RssItem> rssItems) {
                addDataToRssList(rssItems);
                mPostsListView.updateRssList(mRssItems);
                mPostsListView.hideSwipeRefreshProgressBar();
                mPostsListInteractor.saveRssDataToLocalDb(rssItems);
            }

            @Override
            public void onRssOnlineDataLoadFailed() {
                mPostsListView.hideSwipeRefreshProgressBar();
                mPostsListView.showSnackbarMessage(R.string.message_rss_loading_failed);
            }
        });
    }

    /**
     * Sorts rss list to make latest first
     */
    private void sortRssList() {
        Collections.sort(mRssItems, new Comparator<RssItem>() {
            public int compare(RssItem o1, RssItem o2) {
                Date pubDate1 = mDateUtils.convertPubDateStringToDate(o1.getPubDate());
                Date pubDate2 = mDateUtils.convertPubDateStringToDate(o2.getPubDate());
                return pubDate2.compareTo(pubDate1);
            }
        });
    }

    /**
     * Adds rss items to common rss list
     *
     * @param rssItems rss items for adding to common rss list
     */
    private void addDataToRssList(List<RssItem> rssItems) {
        for (RssItem rssItem : rssItems) {
            if (!mRssItems.contains(rssItem))
                mRssItems.add(rssItem);
        }
        sortRssList();
    }

    /**
     * Callback method to listen rss item bitmap loading finished
     *
     * @param rssItem processed rss item
     */
    @Override
    public void onRssBitmapLoaded(RssItem rssItem) {
        mPostsListInteractor.saveRssDataToLocalDb(Collections.singletonList(rssItem));
    }
}
