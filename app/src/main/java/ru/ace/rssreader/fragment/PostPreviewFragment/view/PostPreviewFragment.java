package ru.ace.rssreader.fragment.PostPreviewFragment.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.ace.rssreader.R;
import ru.ace.rssreader.di.components.IPostPreviewFragmentComponent;
import ru.ace.rssreader.di.modules.PostPreviewFragmentModule;
import ru.ace.rssreader.fragment.BaseFragment;
import ru.ace.rssreader.fragment.PostPreviewFragment.presenter.IPostPreviewFragmentPresenter;
import ru.ace.rssreader.router.IMainRouter;

/**
 * Post preview fragment
 */
public class PostPreviewFragment extends BaseFragment implements IPostPreviewFragmentView {

    public static final String TAG = PostPreviewFragment.class.getSimpleName();

    @Inject
    IPostPreviewFragmentPresenter mPostPreviewFargmentPresenter;

    @BindView(R.id.imageView_postPreview)
    ImageView mImageView;

    @BindView(R.id.textView_title)
    TextView mTitleView;

    @BindView(R.id.textView_date)
    TextView mPubDateView;

    @BindView(R.id.textView_author)
    TextView mAuthorView;

    @BindView(R.id.textView_description)
    TextView mDescriptionView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_post_preview, container, false);
        ButterKnife.bind(this, root);
        initDiComponent();

        mPostPreviewFargmentPresenter.loadData();
        return root;
    }

    /**
     * Dependency injection components initialization
     */
    @Override
    public void initDiComponent() {
        IPostPreviewFragmentComponent.PlusComponent activityComponent = getActivityComponent();
        IPostPreviewFragmentComponent component = activityComponent.plusPostPreviewFragmentComponent(new PostPreviewFragmentModule(PostPreviewFragment.this));
        component.inject(this);
    }

    /**
     * Go to PostViewFragment on title text clicked
     */
    @OnClick(R.id.textView_title)
    public void onClick() {
        ((IMainRouter) getActivity()).showPostViewFragment();
    }

    @Override
    public void setBitmap(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
    }

    @Override
    public void setTitle(String title) {
        mTitleView.setText(title);
    }

    @Override
    public void setPublicationDate(String pubDate) {
        mPubDateView.setText(pubDate);
    }

    @Override
    public void setAuthor(String author) {
        mAuthorView.setText(author);
    }

    @Override
    public void setDescription(String description) {
        mDescriptionView.setText(description);
    }
}
