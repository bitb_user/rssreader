package ru.ace.rssreader.fragment.PostPreviewFragment.interactor;

import ru.ace.rssreader.model.RssItem;

/**
 * Post preview fragment interactor interface
 */
public interface IPostPreviewFragmentInteractor {
    RssItem getRssItemById(String rssItemId);
}
