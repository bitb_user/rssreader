package ru.ace.rssreader.fragment.PostsListFragment.interactor;

import java.util.List;

import ru.ace.rssreader.listener.IRssDbLoadListener;
import ru.ace.rssreader.listener.IRssHostCheckListener;
import ru.ace.rssreader.listener.IRssOnlineLoadListener;
import ru.ace.rssreader.model.RssItem;

/**
 * Rss posts list interactor interface
 */
public interface IPostsListInteractor {

    void checkSavedRssHost(IRssHostCheckListener rssHostCheckListener);

    void loadRssCachedData(IRssDbLoadListener rssCacheLoadListener);

    void loadRssOnlineData(IRssOnlineLoadListener rssOnlineLoadListener);

    void saveRssDataToLocalDb(List<RssItem> rssItems);
}
