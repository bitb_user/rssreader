package ru.ace.rssreader.fragment.PostPreviewFragment.interactor;

import javax.inject.Inject;

import ru.ace.rssreader.manager.IDatabaseManager;
import ru.ace.rssreader.model.RssItem;

/**
 * Post preview fragment interactor implementation
 */
public class PostPreviewFragmentInteractorImpl implements IPostPreviewFragmentInteractor {

    @Inject
    IDatabaseManager mDatabaseManager;

    @Inject
    public PostPreviewFragmentInteractorImpl() {
    }

    public RssItem getRssItemById(String rssItemId) {
        return mDatabaseManager.getForId(rssItemId, RssItem.class);
    }
}
