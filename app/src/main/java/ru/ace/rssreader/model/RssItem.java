package ru.ace.rssreader.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

/**
 * Rss item data model class
 */
@DatabaseTable(tableName = "rssItem")
public class RssItem implements Serializable {

    public static final String GUID = "guid";
    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String DESCRIPTION = "description";
    public static final String PUB_DATE = "pubDate";
    public static final String AUTHOR = "author";
    public static final String BITMAP_URL = "bitmapUrl";
    public static final String BITMAP = "bitmap";

    @DatabaseField(id = true)
    private String guid;

    @DatabaseField(columnName = TITLE)
    private String title;

    @DatabaseField(columnName = LINK)
    private String link;

    @DatabaseField(columnName = DESCRIPTION)
    private String description;

    @DatabaseField(columnName = PUB_DATE)
    private String pubDate;

    @DatabaseField(columnName = AUTHOR)
    private String author;

    @DatabaseField(columnName = BITMAP_URL)
    private String bitmapUrl;

    @DatabaseField(dataType = DataType.BYTE_ARRAY, columnName = BITMAP)
    private byte[] thumbnail;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBitmapUrl() {
        return bitmapUrl;
    }

    public void setBitmapUrl(String bitmapUrl) {
        this.bitmapUrl = bitmapUrl;
    }

    public Bitmap getThumbnail() {
        if (thumbnail != null)
            return BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length);
        else
            return null;
    }

    /**
     * Compress bitmap and set to thumbnail
     *
     * @param bitmap original bitmap
     */
    public void setThumbnail(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        thumbnail = outputStream.toByteArray();
    }

    @Override
    public String toString() {
        return "RssItem{" +
                "guid=" + guid +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", pubDate=" + pubDate +
                ", author='" + author + '\'' +
                ", bitmapUrl='" + bitmapUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RssItem rssItem = (RssItem) o;
        return (guid.equals(rssItem.guid));
    }

    @Override
    public int hashCode() {
        return guid.hashCode();
    }
}
