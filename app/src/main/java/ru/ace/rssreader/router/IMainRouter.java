package ru.ace.rssreader.router;

import android.os.Bundle;

/**
 * Main router interface
 */
public interface IMainRouter {

    void showPostPreviewFragment();

    void showPostViewFragment();

    void showSettingsFragment();
}
