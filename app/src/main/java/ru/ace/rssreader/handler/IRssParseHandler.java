package ru.ace.rssreader.handler;

import java.util.List;

import ru.ace.rssreader.model.RssItem;

/**
 * RssParseHandler interface
 */
public interface IRssParseHandler {

    List<RssItem> getRssItems();
}
