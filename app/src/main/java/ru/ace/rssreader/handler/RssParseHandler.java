package ru.ace.rssreader.handler;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import ru.ace.rssreader.model.RssItem;

/**
 * Rss parse handler implementation
 */
public class RssParseHandler extends DefaultHandler implements IRssParseHandler {

    private List<RssItem> mRssItems;
    private RssItem mCurrentRssItem;
    private boolean mInItem = false;
    private StringBuilder mContent;

    public RssParseHandler() {
        mRssItems = new ArrayList<>();
        mContent = new StringBuilder();
    }

    /**
     * @return parsed rss items
     */
    @Override
    public List<RssItem> getRssItems() {
        return mRssItems;
    }

    public void startElement(String uri, String localName, String qName,
                             Attributes atts) throws SAXException {
        mContent = new StringBuilder();
        if (localName.equalsIgnoreCase("item")) {
            mInItem = true;
            mCurrentRssItem = new RssItem();
        } else if (mInItem && localName.equalsIgnoreCase("enclosure")) {
            mCurrentRssItem.setBitmapUrl(atts.getValue("url"));
        }
    }

    public void endElement(String uri, String localName, String qName)
            throws SAXException {

        if (localName.equalsIgnoreCase("title")) {
            if (mInItem) {
                mCurrentRssItem.setTitle(mContent.toString());
            }
        } else if (localName.equalsIgnoreCase("link")) {
            if (mInItem) {
                mCurrentRssItem.setLink(mContent.toString());
            }
        } else if (localName.equalsIgnoreCase("description")) {
            if (mInItem) {
                mCurrentRssItem.setDescription(mContent.toString());
            }
        } else if (mInItem && localName.equalsIgnoreCase("guid")) {
            mCurrentRssItem.setGuid(mContent.toString());
        } else if (mInItem && localName.equalsIgnoreCase("pubDate")) {
            mCurrentRssItem.setPubDate(mContent.toString());
        } else if (mInItem && localName.equalsIgnoreCase("author")) {
            mCurrentRssItem.setAuthor(mContent.toString());
        } else if (localName.equalsIgnoreCase("item")) {
            mInItem = false;
            mRssItems.add(mCurrentRssItem);
        }
    }

    public void characters(char[] ch, int start, int length)
            throws SAXException {
        mContent.append(ch, start, length);
    }
}
