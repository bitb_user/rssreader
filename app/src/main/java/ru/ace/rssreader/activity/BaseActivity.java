package ru.ace.rssreader.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import ru.ace.rssreader.app.RssReaderApp;
import ru.ace.rssreader.di.components.IRssReaderAppComponent;

/**
 * Base activity class
 */
public abstract class BaseActivity extends AppCompatActivity implements IBaseActivity {

    private View mRootView;

    abstract protected void initDiComponent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initDiComponent();
        super.onCreate(savedInstanceState);
        mRootView = findViewById(android.R.id.content);
    }

    /**
     * @return application component with injected modules
     */
    protected IRssReaderAppComponent getAppComponent() {
        return ((RssReaderApp) getApplication()).getComponent();
    }

    @Override
    public void closeSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void showSnackbar(String message, int snackBarLength) {
        Snackbar.make(mRootView, message, snackBarLength)
                .setAction("Action", null).show();
    }
}
