package ru.ace.rssreader.activity.main.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.ace.rssreader.R;
import ru.ace.rssreader.activity.BaseActivity;
import ru.ace.rssreader.di.IHasComponent;
import ru.ace.rssreader.di.components.DaggerIMainActivityComponent;
import ru.ace.rssreader.di.components.IMainActivityComponent;
import ru.ace.rssreader.di.modules.MainActivityModule;
import ru.ace.rssreader.fragment.PostPreviewFragment.view.PostPreviewFragment;
import ru.ace.rssreader.fragment.PostViewFragment.view.PostViewFragment;
import ru.ace.rssreader.fragment.PostsListFragment.view.PostsListFragment;
import ru.ace.rssreader.fragment.SettingsFragment.view.SettingsFragment;
import ru.ace.rssreader.router.IMainRouter;

/**
 * Main activity class
 */

public class MainActivity extends BaseActivity implements IHasComponent<IMainActivityComponent>, IMainView, IMainRouter {

    IMainActivityComponent mMainActivityComponent;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(MainActivity.this);
        setSupportActionBar(mToolbar);
    }

    /**
     * Dependency injection components initialization
     */
    @Override
    public void initDiComponent() {
        mMainActivityComponent = DaggerIMainActivityComponent
                .builder()
                .iRssReaderAppComponent(getAppComponent())
                .mainActivityModule(new MainActivityModule(MainActivity.this))
                .build();
        mMainActivityComponent.inject(MainActivity.this);
    }

    @Override
    public IMainActivityComponent getComponent() {
        return mMainActivityComponent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showSettingsFragment();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Navigation method to show PostPreviewFragment
     */
    @Override
    public void showPostPreviewFragment() {
        if (!isFragmentExists(PostPreviewFragment.TAG)) {
            PostPreviewFragment postPreviewFragment = new PostPreviewFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, postPreviewFragment,
                    PostsListFragment.TAG).addToBackStack(null).commit();
        }
    }

    /**
     * Navigation method to show PostViewFragment
     *
     */
    @Override
    public void showPostViewFragment() {
        if (!isFragmentExists(PostViewFragment.TAG)) {
            PostViewFragment postViewFragment = new PostViewFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, postViewFragment,
                    PostViewFragment.TAG).addToBackStack(null).commit();
        }
    }

    /**
     * Navigation method to show SettingsFragment
     */
    @Override
    public void showSettingsFragment() {
        if (!isFragmentExists(SettingsFragment.TAG)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SettingsFragment(),
                    SettingsFragment.TAG).addToBackStack(null).commit();
        }
    }

    /**
     * Checks for existing fragment
     *
     * @param tag needed fragment tag
     * @return fragment existing
     */
    private boolean isFragmentExists(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        return fragment != null;
    }
}
