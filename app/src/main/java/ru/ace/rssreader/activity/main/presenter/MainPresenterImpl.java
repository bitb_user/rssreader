package ru.ace.rssreader.activity.main.presenter;

import javax.inject.Inject;

import ru.ace.rssreader.activity.main.interactor.IMainInteractor;
import ru.ace.rssreader.activity.main.view.IMainView;

/**
 * Main activity presenter implementation
 */
public class MainPresenterImpl implements IMainPresenter {

    private IMainView mMainView;
    private IMainInteractor mMainInteractor;

    @Inject
    public MainPresenterImpl(IMainView mainView, IMainInteractor mainInteractor) {
        mMainView = mainView;
        mMainInteractor = mainInteractor;
    }
}
