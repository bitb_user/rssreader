package ru.ace.rssreader.activity;

/**
 * Base activity interface
 */
public interface IBaseActivity {

    void closeSoftKeyboard();

    void showSnackbar(String message, int snackBarLength);
}
