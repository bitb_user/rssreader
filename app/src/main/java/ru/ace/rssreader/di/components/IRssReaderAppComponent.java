package ru.ace.rssreader.di.components;

import android.content.Context;

import dagger.Component;
import ru.ace.rssreader.app.RssReaderApp;
import ru.ace.rssreader.util.IDateUtils;
import ru.ace.rssreader.util.INetUtils;
import ru.ace.rssreader.di.modules.DatabaseManagerModule;
import ru.ace.rssreader.di.modules.RssFeedManagerModule;
import ru.ace.rssreader.di.modules.RssHostManagerModule;
import ru.ace.rssreader.di.modules.RssParseHandlerModule;
import ru.ace.rssreader.di.modules.RssReaderAppModule;
import ru.ace.rssreader.di.modules.SharedPrefsManagerModule;
import ru.ace.rssreader.di.modules.UtilsModule;
import ru.ace.rssreader.di.scopes.PerApplication;
import ru.ace.rssreader.handler.IRssParseHandler;
import ru.ace.rssreader.manager.IDatabaseManager;
import ru.ace.rssreader.manager.IRssFeedManager;
import ru.ace.rssreader.manager.IRssHostManager;
import ru.ace.rssreader.manager.ISharedPrefsManager;

/**
 * Application component
 */
@PerApplication
@Component(modules = {

        RssReaderAppModule.class,
        SharedPrefsManagerModule.class,
        DatabaseManagerModule.class,
        UtilsModule.class,
        RssParseHandlerModule.class,
        RssFeedManagerModule.class,
        RssHostManagerModule.class
})

public interface IRssReaderAppComponent {

    Context context();

    ISharedPrefsManager sharedPrefManager();

    IDatabaseManager databaseManager();

    IRssParseHandler rssParseHandler();

    IRssFeedManager rssFeedManager();

    IRssHostManager rssHostManager();

    IDateUtils dateUtils();

    INetUtils netUtils();

    void inject(RssReaderApp rssReaderApp);
}
