package ru.ace.rssreader.di.components;

import dagger.Subcomponent;
import ru.ace.rssreader.di.modules.PostPreviewFragmentModule;
import ru.ace.rssreader.di.scopes.PerFragment;
import ru.ace.rssreader.fragment.PostPreviewFragment.view.PostPreviewFragment;

/**
 * Post preview fragment component
 */
@PerFragment
@Subcomponent(modules = PostPreviewFragmentModule.class)
public interface IPostPreviewFragmentComponent {

    void inject(PostPreviewFragment postPreviewFragment);

    interface PlusComponent {
        IPostPreviewFragmentComponent plusPostPreviewFragmentComponent(PostPreviewFragmentModule postPreviewFragmentModule);
    }
}
