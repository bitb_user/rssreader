package ru.ace.rssreader.di.components;

import dagger.Subcomponent;
import ru.ace.rssreader.di.scopes.PerApplication;
import ru.ace.rssreader.manager.IRssFeedManager;

/**
 * Rss feed manager component
 */
@Subcomponent
@PerApplication
public interface IRssFeedManagerComponent {

    void inject(IRssFeedManager rssFeedManager);
}
