package ru.ace.rssreader.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerFragment;
import ru.ace.rssreader.fragment.SettingsFragment.interactor.ISettingsFragmentInteractor;
import ru.ace.rssreader.fragment.SettingsFragment.interactor.SettingsFragmentInteractorImpl;
import ru.ace.rssreader.fragment.SettingsFragment.presenter.ISettingsFragmentPresenter;
import ru.ace.rssreader.fragment.SettingsFragment.presenter.SettingsFragmentPresenterImpl;
import ru.ace.rssreader.fragment.SettingsFragment.view.ISettingsFragmentView;

/**
 * Settings fragment module
 */
@Module
public class SettingsFragmentModule {

    private ISettingsFragmentView mSettingsFragmentView;

    public SettingsFragmentModule(ISettingsFragmentView settingsFragmentView) {
        mSettingsFragmentView = settingsFragmentView;
    }

    @Provides
    @PerFragment
    ISettingsFragmentView settingsFragmentView() {
        return mSettingsFragmentView;
    }

    @Provides
    @PerFragment
    ISettingsFragmentPresenter postsListPresenter(SettingsFragmentPresenterImpl settingsFragmentPresenter) {
        return settingsFragmentPresenter;
    }

    @Provides
    @PerFragment
    ISettingsFragmentInteractor settingsFragmentInteractor(SettingsFragmentInteractorImpl settingsFragmentInteractor) {
        return settingsFragmentInteractor;
    }
}
