package ru.ace.rssreader.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerApplication;
import ru.ace.rssreader.handler.IRssParseHandler;
import ru.ace.rssreader.handler.RssParseHandler;

/**
 * Rss parse handel module
 */
@Module
public class RssParseHandlerModule {

    @Provides
    @PerApplication
    public IRssParseHandler rssParseHandler() {
        return new RssParseHandler();
    }

}
