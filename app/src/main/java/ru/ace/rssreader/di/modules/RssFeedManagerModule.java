package ru.ace.rssreader.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerApplication;
import ru.ace.rssreader.manager.IRssFeedManager;
import ru.ace.rssreader.manager.RssFeedManager;

/**
 * Rss feed manager module
 */
@Module
public class RssFeedManagerModule {

    private Context mContext;

    public RssFeedManagerModule(Context context) {
        mContext = context;
    }

    @Provides
    @PerApplication
    IRssFeedManager rssService() {
        return new RssFeedManager(mContext);
    }
}
