package ru.ace.rssreader.di.components;

import dagger.Subcomponent;
import ru.ace.rssreader.di.modules.PostViewFragmentModule;
import ru.ace.rssreader.di.scopes.PerFragment;
import ru.ace.rssreader.fragment.PostViewFragment.view.PostViewFragment;

/**
 * Post view fragment component
 */
@PerFragment
@Subcomponent(modules = PostViewFragmentModule.class)
public interface IPostViewFragmentComponent {

    void inject(PostViewFragment postViewFragment);

    interface PlusComponent {
        IPostViewFragmentComponent plusPostViewFragmentComponent(PostViewFragmentModule postViewFragmentModule);
    }
}