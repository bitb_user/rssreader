package ru.ace.rssreader.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerFragment;
import ru.ace.rssreader.fragment.PostPreviewFragment.view.PostPreviewFragment;
import ru.ace.rssreader.fragment.PostsListFragment.interactor.IPostsListInteractor;
import ru.ace.rssreader.fragment.PostsListFragment.interactor.PostsListInteractorImpl;
import ru.ace.rssreader.fragment.PostsListFragment.presenter.IPostsListPresenter;
import ru.ace.rssreader.fragment.PostsListFragment.presenter.PostsListPresenterImpl;
import ru.ace.rssreader.fragment.PostsListFragment.view.IPostsListView;

/**
 * Created by User on 20.05.2016.
 */

@Module
public class PostsListFragmentModule {

    private IPostsListView postsListView;

    public PostsListFragmentModule(IPostsListView postsListView) {
        this.postsListView = postsListView;
    }

    @Provides
    @PerFragment
    IPostsListView postsListView() {
        return this.postsListView;
    }

    @Provides
    @PerFragment
    IPostsListPresenter postsListPresenter(PostsListPresenterImpl postsListPresenter) {
        return postsListPresenter;
    }

    @Provides
    @PerFragment
    IPostsListInteractor postsListInteractor(PostsListInteractorImpl postsListInteractor) {
        return postsListInteractor;
    }

    @Provides
    @PerFragment
    PostPreviewFragment postPreviewFragment(){return new PostPreviewFragment();}
}
