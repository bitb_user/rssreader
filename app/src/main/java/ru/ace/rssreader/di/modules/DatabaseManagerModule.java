package ru.ace.rssreader.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerApplication;
import ru.ace.rssreader.manager.DatabaseManager;
import ru.ace.rssreader.manager.IDatabaseManager;

/**
 * Database manager module
 */
@Module
public class DatabaseManagerModule {

    private Context mContext;

    public DatabaseManagerModule(Context context) {
        mContext = context;
    }

    @PerApplication
    @Provides
    public IDatabaseManager localDatabaseService() {
        return new DatabaseManager(mContext);
    }
}
