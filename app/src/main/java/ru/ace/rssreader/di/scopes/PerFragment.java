package ru.ace.rssreader.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Per fragment scope
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerFragment {
}
