package ru.ace.rssreader.di;

public interface IHasComponent<T> {

    T getComponent();

}
