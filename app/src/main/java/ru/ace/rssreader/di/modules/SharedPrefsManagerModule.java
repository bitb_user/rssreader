package ru.ace.rssreader.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerApplication;
import ru.ace.rssreader.manager.ISharedPrefsManager;
import ru.ace.rssreader.manager.SharedPrefsManager;

/**
 * Shared preferences manager module
 */
@Module
public class SharedPrefsManagerModule {

    private Context mContext;

    public SharedPrefsManagerModule(Context context) {
        mContext = context;
    }

    @Provides
    @PerApplication
    ISharedPrefsManager sharedPrefsService() {
        return new SharedPrefsManager(mContext);
    }
}
