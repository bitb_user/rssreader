package ru.ace.rssreader.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerApplication;
import ru.ace.rssreader.manager.IRssHostManager;
import ru.ace.rssreader.manager.RssHostManager;

/**
 * Rss host manager module
 */
@Module
public class RssHostManagerModule {

    private Context mContext;

    public RssHostManagerModule(Context context) {
        mContext = context;
    }

    @Provides
    @PerApplication
    public IRssHostManager rssHostManager() {
        return new RssHostManager(mContext);
    }
}
