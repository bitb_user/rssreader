package ru.ace.rssreader.di.components;

import dagger.Component;
import ru.ace.rssreader.activity.main.view.MainActivity;
import ru.ace.rssreader.di.modules.MainActivityModule;
import ru.ace.rssreader.di.scopes.PerActivity;

/**
 * Main activity component
 */
@PerActivity
@Component(modules = MainActivityModule.class, dependencies = IRssReaderAppComponent.class)
public interface IMainActivityComponent extends
        IPostsListFragmentComponent.PlusComponent,
        IPostPreviewFragmentComponent.PlusComponent,
        IPostViewFragmentComponent.PlusComponent,
        ISettingsFragmentComponent.PlusComponent {

    void inject(MainActivity mainActivity);
}
