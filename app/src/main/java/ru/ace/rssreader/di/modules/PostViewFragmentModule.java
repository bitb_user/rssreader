package ru.ace.rssreader.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerFragment;
import ru.ace.rssreader.fragment.PostViewFragment.interactor.IPostViewInteractor;
import ru.ace.rssreader.fragment.PostViewFragment.interactor.PostViewInteractorImpl;
import ru.ace.rssreader.fragment.PostViewFragment.presenter.IPostViewPresenter;
import ru.ace.rssreader.fragment.PostViewFragment.presenter.PostViewPresenterImpl;
import ru.ace.rssreader.fragment.PostViewFragment.view.IPostViewFragmentView;

/**
 * Post view fragment module
 */
@Module
public class PostViewFragmentModule {

    private IPostViewFragmentView mPostViewFragmentView;

    public PostViewFragmentModule(IPostViewFragmentView postViewFragmentView) {
        mPostViewFragmentView = postViewFragmentView;
    }

    @Provides
    @PerFragment
    IPostViewFragmentView postViewFragmentView() {
        return mPostViewFragmentView;
    }

    @Provides
    @PerFragment
    IPostViewPresenter postViewPresenter(PostViewPresenterImpl postViewPresenter) {
        return postViewPresenter;
    }

    @Provides
    @PerFragment
    IPostViewInteractor postViewInteractor(PostViewInteractorImpl postViewInteractor) {
        return postViewInteractor;
    }

}
