package ru.ace.rssreader.di.components;

import dagger.Subcomponent;
import ru.ace.rssreader.di.modules.PostsListFragmentModule;
import ru.ace.rssreader.di.scopes.PerFragment;
import ru.ace.rssreader.fragment.PostsListFragment.view.PostsListFragment;

/**
 * Posts list fragment component
 */

@PerFragment
@Subcomponent(modules = PostsListFragmentModule.class)
public interface IPostsListFragmentComponent {

    void inject(PostsListFragment postsListsFragment);

    interface PlusComponent {
        IPostsListFragmentComponent plusIPostsListFragmentComponent(PostsListFragmentModule postsListFragmentModule);
    }
}
