package ru.ace.rssreader.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerApplication;

/**
 * Application module
 */
@Module
public class RssReaderAppModule {

    protected final Context mAppContext;

    public RssReaderAppModule(@NonNull Context appContext) {
        mAppContext = appContext;
    }

    @Provides
    @PerApplication
    public Context applicationContext() {
        return mAppContext;
    }
}
