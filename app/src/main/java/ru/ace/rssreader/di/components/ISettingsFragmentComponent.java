package ru.ace.rssreader.di.components;

import dagger.Subcomponent;
import ru.ace.rssreader.di.modules.SettingsFragmentModule;
import ru.ace.rssreader.di.scopes.PerFragment;
import ru.ace.rssreader.fragment.SettingsFragment.view.SettingsFragment;

/**
 * Settings fragment component
 */
@PerFragment
@Subcomponent(modules = SettingsFragmentModule.class)
public interface ISettingsFragmentComponent {

    void inject(SettingsFragment settingsFragment);

    interface PlusComponent {
        ISettingsFragmentComponent plusISettingsFragmentComponent(SettingsFragmentModule settingsFragmentModule);
    }

}
