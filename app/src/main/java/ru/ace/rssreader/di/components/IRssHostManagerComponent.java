package ru.ace.rssreader.di.components;

import dagger.Subcomponent;
import ru.ace.rssreader.di.scopes.PerApplication;
import ru.ace.rssreader.manager.IRssHostManager;

/**
 * Rss host manager component
 */
@Subcomponent
@PerApplication
public interface IRssHostManagerComponent {

    void inject(IRssHostManager rssHostManager);
}
