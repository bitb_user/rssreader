package ru.ace.rssreader.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.di.scopes.PerFragment;
import ru.ace.rssreader.fragment.PostPreviewFragment.interactor.IPostPreviewFragmentInteractor;
import ru.ace.rssreader.fragment.PostPreviewFragment.interactor.PostPreviewFragmentInteractorImpl;
import ru.ace.rssreader.fragment.PostPreviewFragment.presenter.IPostPreviewFragmentPresenter;
import ru.ace.rssreader.fragment.PostPreviewFragment.presenter.PostPreviewFragmentPresenterImpl;
import ru.ace.rssreader.fragment.PostPreviewFragment.view.IPostPreviewFragmentView;

/**
 * Post preview fragment module
 */
@Module
public class PostPreviewFragmentModule {

    private IPostPreviewFragmentView mPostPreviewFragmentView;

    public PostPreviewFragmentModule(IPostPreviewFragmentView postPreviewFragmentView) {
        mPostPreviewFragmentView = postPreviewFragmentView;
    }

    @Provides
    @PerFragment
    IPostPreviewFragmentView postPreviewFragmentView() {
        return mPostPreviewFragmentView;
    }

    @Provides
    @PerFragment
    IPostPreviewFragmentPresenter postPreviewFragmentPresenter(PostPreviewFragmentPresenterImpl postPreviewFragmentPresenter) {
        return postPreviewFragmentPresenter;
    }

    @Provides
    @PerFragment
    IPostPreviewFragmentInteractor postPreviewFragmentInteractor(PostPreviewFragmentInteractorImpl postPreviewFragmentInteractor) {
        return postPreviewFragmentInteractor;
    }

}
