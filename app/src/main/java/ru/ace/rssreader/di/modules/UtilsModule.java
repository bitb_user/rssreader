package ru.ace.rssreader.di.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.util.DateUtils;
import ru.ace.rssreader.util.IDateUtils;
import ru.ace.rssreader.util.INetUtils;
import ru.ace.rssreader.util.NetUtils;
import ru.ace.rssreader.di.scopes.PerApplication;

@Module
public class UtilsModule {

    private Context mContext;

    public UtilsModule(Context context) {
        mContext = context;
    }


    @Provides
    @PerApplication
    IDateUtils dateUtils() {
        return new DateUtils(mContext);
    }

    @Provides
    @PerApplication
    INetUtils netUtils() {
        return new NetUtils();
    }
}
