package ru.ace.rssreader.di.modules;

import dagger.Module;
import dagger.Provides;
import ru.ace.rssreader.activity.main.interactor.IMainInteractor;
import ru.ace.rssreader.activity.main.interactor.MainInteractorImpl;
import ru.ace.rssreader.activity.main.presenter.IMainPresenter;
import ru.ace.rssreader.activity.main.presenter.MainPresenterImpl;
import ru.ace.rssreader.activity.main.view.IMainView;
import ru.ace.rssreader.di.scopes.PerActivity;

/**
 * Main activity module
 */
@Module
public class MainActivityModule {

    private IMainView mMainView;

    public MainActivityModule(IMainView mainView) {
        mMainView = mainView;
    }

    @Provides
    @PerActivity
    IMainView mainView() {
        return mMainView;
    }

    @Provides
    @PerActivity
    IMainInteractor mainInteractor(MainInteractorImpl mainInteractor) {
        return mainInteractor;
    }

    @Provides
    @PerActivity
    IMainPresenter mainPresenter(MainPresenterImpl mainPresenter) {
        return mainPresenter;
    }
}
