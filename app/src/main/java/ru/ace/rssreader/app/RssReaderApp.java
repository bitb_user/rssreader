package ru.ace.rssreader.app;

import android.app.Application;

import ru.ace.rssreader.di.IHasComponent;
import ru.ace.rssreader.di.components.DaggerIRssReaderAppComponent;
import ru.ace.rssreader.di.components.IRssReaderAppComponent;
import ru.ace.rssreader.di.modules.DatabaseManagerModule;
import ru.ace.rssreader.di.modules.RssFeedManagerModule;
import ru.ace.rssreader.di.modules.RssHostManagerModule;
import ru.ace.rssreader.di.modules.RssReaderAppModule;
import ru.ace.rssreader.di.modules.SharedPrefsManagerModule;
import ru.ace.rssreader.di.modules.UtilsModule;

/**
 * Application class
 */
public class RssReaderApp extends Application implements IHasComponent<IRssReaderAppComponent> {

    private IRssReaderAppComponent rssReaderAppComponent;

    public void onCreate() {
        super.onCreate();
        initDiComponent();
    }

    /**
     * Dependency injection components initialization
     */
    public void initDiComponent() {
        rssReaderAppComponent = DaggerIRssReaderAppComponent
                .builder()
                .rssReaderAppModule(new RssReaderAppModule(getApplicationContext()))
                .sharedPrefsManagerModule(new SharedPrefsManagerModule(this))
                .databaseManagerModule(new DatabaseManagerModule(this))
                .rssFeedManagerModule(new RssFeedManagerModule(this))
                .rssHostManagerModule(new RssHostManagerModule(this))
                .utilsModule(new UtilsModule(this))
                .build();
        rssReaderAppComponent.inject(this);
    }

    /**
     * @return initialized application component
     */
    @Override
    public IRssReaderAppComponent getComponent() {
        return rssReaderAppComponent;
    }
}
