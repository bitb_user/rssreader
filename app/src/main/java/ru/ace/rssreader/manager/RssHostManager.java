package ru.ace.rssreader.manager;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import ru.ace.rssreader.Constants;
import ru.ace.rssreader.app.RssReaderApp;
import ru.ace.rssreader.util.INetUtils;
import ru.ace.rssreader.di.components.IRssReaderAppComponent;
import ru.ace.rssreader.listener.IRssHostCheckListener;

/**
 * Rss host manager implementation
 */
public class RssHostManager implements IRssHostManager {

    public interface HostStatus {
        int VALID = 0;
        int INVALID = 1;
        int NOT_SPECIFIED = 2;
    }

    private static final String TAG = RssHostManager.class.getSimpleName();

    private Context mContext;
    private ISharedPrefsManager mSharedPrefsManager;
    private INetUtils mNetUtils;

    public RssHostManager(Context context) {
        mContext = context;
        initDi();
    }

    /**
     * Components initialization
     */
    private void initDi() {
        IRssReaderAppComponent appComponent = ((RssReaderApp) mContext.getApplicationContext()).getComponent();
        mSharedPrefsManager = appComponent.sharedPrefManager();
        mNetUtils = appComponent.netUtils();
    }

    /**
     * Saves rss host url to shared preferences
     *
     * @param rssHost url to save
     */
    @Override
    public void saveRssHost(String rssHost) {
        mSharedPrefsManager.setString(Constants.SharedPrefs.RSS_HOST_KEY, rssHost);
    }

    /**
     * @return saved rss host url from shared preferences
     */
    @Override
    public String getSavedRssHost() {
        return mSharedPrefsManager.getString(Constants.SharedPrefs.RSS_HOST_KEY, "");
    }

    /**
     * Async checks rss host validity
     *
     * @param hostUrl              rss host url to check
     * @param rssHostCheckListener on rss host check finish listener
     */
    @Override
    public void checkRssHost(final String hostUrl, final IRssHostCheckListener rssHostCheckListener) {
        new AsyncTask<Void, Void, Integer>() {

            @Override
            protected void onPreExecute() {
                if (!mNetUtils.isOnline(mContext)) {
                    rssHostCheckListener.onNetUnavailable();
                    this.cancel(true);
                }
            }

            @Override
            protected Integer doInBackground(Void... params) {
                try {
                    URL url = new URL(hostUrl);
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document doc = db.parse(new InputSource(url.openStream()));
                    doc.getDocumentElement().normalize();

                    Element root = doc.getDocumentElement();
                    String rootTagName = root.getTagName();

                    if (rootTagName.equals("")) {
                        return HostStatus.NOT_SPECIFIED;
                    } else if (rootTagName.equalsIgnoreCase("rss")) {
                        return HostStatus.VALID;
                    } else {
                        return HostStatus.INVALID;
                    }

                } catch (Exception e) {
                    Log.w(TAG, "Exception while rss host url checking", e);
                    return HostStatus.INVALID;
                }
            }

            @Override
            protected void onPostExecute(Integer hostStatus) {
                rssHostCheckListener.onCheckFinish(hostStatus);
            }
        }.execute();
    }
}
