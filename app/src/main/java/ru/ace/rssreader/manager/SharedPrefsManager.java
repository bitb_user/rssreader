package ru.ace.rssreader.manager;

import android.content.Context;
import android.content.SharedPreferences;

import ru.ace.rssreader.Constants;

/**
 * Shared preferences manager implementation
 */
public class SharedPrefsManager implements ISharedPrefsManager {

    private Context mContext;

    public SharedPrefsManager(Context context) {
        mContext = context;
    }

    @Override
    public int getInt(String key, int defValue) {
        return getSP().getInt(key, defValue);
    }

    @Override
    public void setInt(String key, int value) {
        getSP().edit().putInt(key, value).apply();
    }

    @Override
    public boolean getBool(String key, boolean defValue) {
        return getSP().getBoolean(key, defValue);
    }

    @Override
    public void setBool(String key, boolean value) {
        getSP().edit().putBoolean(key, value).apply();
    }

    @Override
    public float getFloat(String key, float defValue) {
        return getSP().getFloat(key, defValue);
    }

    @Override
    public void setFloat(String key, float value) {
        getSP().edit().putFloat(key, value).apply();
    }

    @Override
    public String getString(String key, String defValue) {
        return getSP().getString(key, defValue);
    }

    @Override
    public void setString(String key, String value) {
        getSP().edit().putString(key, value).apply();
    }

    @Override
    public SharedPreferences getSP() {
        return mContext.getSharedPreferences(Constants.SharedPrefs.SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }
}
