package ru.ace.rssreader.manager;

import java.util.List;

/**
 * Database manager interface
 */
public interface IDatabaseManager {

    <T> void create(final T data, Class<T> clazz);

    <T> void createOrUpdate(final T data, Class<T> clazz);

    <T> void createOrUpdate(final List<T> data, Class<T> clazz);

    <T, E> T getForId(E id, Class<T> clazz);

    <T> List<T> getList(Class<T> clazz);

    <T> List<String> getListField(Class<T> clazz, String fieldName);

    <T> void removeObjectsByColumn(final String columnName, final Object value, Class<T> clazz);

    <T> void clearTable(Class<T> clazz);

}
