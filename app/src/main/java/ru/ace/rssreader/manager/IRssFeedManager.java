package ru.ace.rssreader.manager;

import java.util.List;

import ru.ace.rssreader.listener.IRssDbLoadListener;
import ru.ace.rssreader.listener.IRssOnlineLoadListener;
import ru.ace.rssreader.model.RssItem;

/**
 * Rss feed manager interface
 */
public interface IRssFeedManager {

    void loadCachedRssData(IRssDbLoadListener rssCacheLoadListener);

    void loadOnlineRssData(IRssOnlineLoadListener rssOnlineLoadListener);

    void saveRssDataToLocalDb(List<RssItem> rssItems);
}
