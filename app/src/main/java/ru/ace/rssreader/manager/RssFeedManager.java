package ru.ace.rssreader.manager;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.net.URL;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import ru.ace.rssreader.Constants;
import ru.ace.rssreader.app.RssReaderApp;
import ru.ace.rssreader.di.components.IRssReaderAppComponent;
import ru.ace.rssreader.handler.IRssParseHandler;
import ru.ace.rssreader.listener.IRssDbLoadListener;
import ru.ace.rssreader.listener.IRssOnlineLoadListener;
import ru.ace.rssreader.model.RssItem;

/**
 * Rss feed manager implementation
 */
public class RssFeedManager implements IRssFeedManager {

    private static final String TAG = RssFeedManager.class.getSimpleName();

    private ISharedPrefsManager mSharedPrefsManager;
    private IDatabaseManager mDatabaseManager;
    private IRssParseHandler mRssParseHandler;

    public RssFeedManager(Context context) {
        initDi(context);
    }

    /**
     * Components initialization
     *
     * @param context current context
     */
    private void initDi(Context context) {
        IRssReaderAppComponent appComponent = ((RssReaderApp) context.getApplicationContext()).getComponent();
        mSharedPrefsManager = appComponent.sharedPrefManager();
        mDatabaseManager = appComponent.databaseManager();
        mRssParseHandler = appComponent.rssParseHandler();
    }

    /**
     * Async loads saved in db rss data
     *
     * @param rssCacheLoadListener on saved rss data load finish listener
     */
    @Override
    public void loadCachedRssData(final IRssDbLoadListener rssCacheLoadListener) {
        new AsyncTask<Void, Void, List<RssItem>>() {

            @Override
            protected List<RssItem> doInBackground(Void... params) {
                try {
                    return mDatabaseManager.getList(RssItem.class);

                } catch (Exception e) {
                    Log.w(TAG, "Exception while loading cached rss data");
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<RssItem> rssItems) {
                if (rssItems == null) {
                    rssCacheLoadListener.onRssCacheLoadedFailed();
                }
                rssCacheLoadListener.onSavedRssLoaded(rssItems);
            }
        }.execute();
    }

    /**
     * Async loads online rss data
     *
     * @param rssOnlineLoadListener on online rss data load finish listener
     */
    @Override
    public void loadOnlineRssData(final IRssOnlineLoadListener rssOnlineLoadListener) {
        new AsyncTask<Void, Void, List<RssItem>>() {

            @Override
            protected List<RssItem> doInBackground(Void... params) {
                try {
                    SAXParserFactory saxPF = SAXParserFactory.newInstance();
                    SAXParser saxP = saxPF.newSAXParser();
                    XMLReader xmlR = saxP.getXMLReader();

                    //Get rss host url from Shared preferences
                    String rssHost = mSharedPrefsManager.getString(Constants.SharedPrefs.RSS_HOST_KEY, "");
                    URL url = new URL(rssHost);

                    //Parse rss stream
                    xmlR.setContentHandler((ContentHandler) mRssParseHandler);
                    xmlR.parse(new InputSource(url.openStream()));
                    return mRssParseHandler.getRssItems();

                } catch (Exception e) {
                    Log.w(TAG, "Exception while loading rss data from host");
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<RssItem> rssItems) {
                if (rssItems == null) {
                    rssOnlineLoadListener.onRssOnlineDataLoadFailed();
                } else {
                    rssOnlineLoadListener.onRssOnlineDataLoaded(rssItems);
                }
            }
        }.execute();
    }

    /**
     * Async saves rss items to local database
     *
     * @param rssItems rss items to save
     */
    @Override
    public void saveRssDataToLocalDb(final List<RssItem> rssItems) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                mDatabaseManager.createOrUpdate(rssItems, RssItem.class);
                return null;
            }
        }.execute();
    }
}
