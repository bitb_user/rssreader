package ru.ace.rssreader.manager;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import ru.ace.rssreader.db.DatabaseHelper;

/**
 * Database manager implementation
 */
public class DatabaseManager implements IDatabaseManager {

    private final String TAG = DatabaseManager.class.getSimpleName();

    private DatabaseHelper mDatabaseHelper;

    public DatabaseManager(Context context) {
        mDatabaseHelper = new DatabaseHelper(context);
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // ADD METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public <T> void create(final T data, Class<T> clazz) {
        if (data == null)
            return;

        try {
            final Dao<T, ?> dao = mDatabaseHelper.getDao(clazz);
            dao.create(data);
        } catch (Exception e) {
            Log.e(TAG, "Exception while creating db object", e);
        }
    }

    @Override
    public <T> void createOrUpdate(final T data, Class<T> clazz) {
        if (data == null)
            return;

        try {
            final Dao<T, ?> dao = mDatabaseHelper.getDao(clazz);
            dao.createOrUpdate(data);
        } catch (Exception e) {
            Log.e(TAG, "Exception while creating or updating db object", e);
        }
    }

    @Override
    public <T> void createOrUpdate(final List<T> data, Class<T> clazz) {
        if (data == null || data.isEmpty())
            return;

        try {
            final Dao<T, Integer> dao = mDatabaseHelper.getDao(clazz);
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (T item : data) {
                        dao.createOrUpdate(item);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "Exception while creating or updating list of db objects", e);
        }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // EXTRACT METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public <T, E> T getForId(E id, Class<T> clazz) {
        try {
            Dao<T, E> dao = mDatabaseHelper.getDao(clazz);
            return dao.queryForId(id);
        } catch (Exception e) {
            Log.e(TAG, "Exception while getting db object by id", e);
            return null;
        }
    }

    @Override
    public <T> List<T> getList(Class<T> clazz) {
        List<T> result;
        try {
            Dao<T, ?> dao = mDatabaseHelper.getDao(clazz);
            result = dao.queryForAll();
        } catch (Exception e) {
            Log.e(TAG, "Exception while getting list of db objects", e);
            return Collections.emptyList();
        }
        return result;
    }

    @Override
    public <T> List<String> getListField(Class<T> clazz, String fieldName) {
        List<String> result = new ArrayList<String>();
        try {
            Dao<T, ?> dao = mDatabaseHelper.getDao(clazz);
            GenericRawResults<String[]> raws = dao.queryRaw("SELECT DISTINCT " + fieldName
                    + " FROM " + clazz.getSimpleName());

            for (String[] strings : raws)
                result.add(strings[0]);

        } catch (Exception e) {
            Log.e(TAG, "Exception while getting field list of db objects", e);
            return Collections.emptyList();
        }
        return result;
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////
    // DELETE METHODS
    // ////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public <T> void removeObjectsByColumn(final String columnName, final Object value,
                                          Class<T> clazz) {
        if (columnName == null)
            return;
        try {
            final Dao<T, String> dao = mDatabaseHelper.getDao(clazz);
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    DeleteBuilder<T, String> deleteBuilder = dao.deleteBuilder();
                    deleteBuilder.where().eq(columnName, value);
                    deleteBuilder.delete();
                    return null;
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "Exception while removing db object by column", e);
        }
    }

    @Override
    public <T> void clearTable(Class<T> clazz) {
        try {
            TableUtils.clearTable(mDatabaseHelper.getConnectionSource(), clazz);
        } catch (SQLException e) {
            Log.e(TAG, "Exception while clearing db table", e);
        }
    }
}
