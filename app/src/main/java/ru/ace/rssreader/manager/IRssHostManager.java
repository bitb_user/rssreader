package ru.ace.rssreader.manager;

import ru.ace.rssreader.listener.IRssHostCheckListener;

/**
 * Rss host manaager interface
 */
public interface IRssHostManager {

    void checkRssHost(String hostUrl, IRssHostCheckListener rssHostCheckListener);

    void saveRssHost(String hostUrl);

    String getSavedRssHost();
}
