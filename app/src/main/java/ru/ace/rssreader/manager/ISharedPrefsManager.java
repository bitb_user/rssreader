package ru.ace.rssreader.manager;

import android.content.SharedPreferences;

/**
 * Shared preference manager interface
 */
public interface ISharedPrefsManager {

    int getInt(String key, int defValue);

    void setInt(String key, int value);

    boolean getBool(String key, boolean defValue);

    void setBool(String key, boolean value);

    float getFloat(String key, float defValue);

    void setFloat(String key, float value);

    String getString(String key, String defValue);

    void setString(String key, String value);

    SharedPreferences getSP();
}
