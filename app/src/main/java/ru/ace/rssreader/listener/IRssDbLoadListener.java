package ru.ace.rssreader.listener;

import java.util.List;

import ru.ace.rssreader.model.RssItem;

/**
 * Rss items load from db listener
 */
public interface IRssDbLoadListener {
    void onSavedRssLoaded(List<RssItem> rssItems);

    void onRssCacheLoadedFailed();
}
