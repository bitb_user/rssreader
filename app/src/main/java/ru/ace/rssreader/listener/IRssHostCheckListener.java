package ru.ace.rssreader.listener;

/**
 * Rss url host check listener
 */
public interface IRssHostCheckListener {
    void onNetUnavailable();

    void onCheckFinish(int hostStatus);
}
