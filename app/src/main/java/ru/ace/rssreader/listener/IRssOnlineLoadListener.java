package ru.ace.rssreader.listener;

import java.util.List;

import ru.ace.rssreader.model.RssItem;

/**
 * Rss items online load listener
 */
public interface IRssOnlineLoadListener {

    void onRssOnlineDataLoaded(List<RssItem> rssItems);

    void onRssOnlineDataLoadFailed();
}
