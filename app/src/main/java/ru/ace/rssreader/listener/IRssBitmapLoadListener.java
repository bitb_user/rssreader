package ru.ace.rssreader.listener;

import ru.ace.rssreader.model.RssItem;

/**
 * Rss item bitmap load listener
 */
public interface IRssBitmapLoadListener {

    void onRssBitmapLoaded(RssItem rssItem);
}
