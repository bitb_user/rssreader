package ru.ace.rssreader.listener;

import ru.ace.rssreader.model.RssItem;

/**
 * On rss item select listener
 */
public interface OnSelectRssItemListener {
    void onSelectRssItem(String rssItemId);
}
