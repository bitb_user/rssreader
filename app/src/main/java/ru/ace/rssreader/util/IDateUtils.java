package ru.ace.rssreader.util;

import java.util.Date;

/**
 * Date utilities interface
 */
public interface IDateUtils {

    String convertPubDateToUserFormat(String rssItemPubDate);

    Date convertPubDateStringToDate(String dbPubDate);
}
