package ru.ace.rssreader.util;

import android.content.Context;

/**
 * Net utilities interface
 */
public interface INetUtils {
    boolean isOnline(Context context);
}
