package ru.ace.rssreader.util;

import android.content.Context;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Date utilities implementation
 */
public class DateUtils implements IDateUtils {

    private Context mContext;

    public DateUtils(Context context) {
        mContext = context;
    }

    /**
     * Converts rss publication date string to user format string
     *
     * @param rssItemPubDate rss publication date string
     * @return user formatted publication date string
     */
    @Override
    public String convertPubDateToUserFormat(String rssItemPubDate) {
        Date pubDate = convertPubDateStringToDate(rssItemPubDate);

        //Get user date format
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(mContext);

        //Get user time format
        DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(mContext);

        return dateFormat.format(pubDate) + " " + timeFormat.format(pubDate);
    }

    /**
     * Converts rss publication date string to Date
     *
     * @param pubDate publication date string
     * @return publication date
     */
    @Override
    public Date convertPubDateStringToDate(String pubDate) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
            return dateFormat.parse(pubDate);

        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
