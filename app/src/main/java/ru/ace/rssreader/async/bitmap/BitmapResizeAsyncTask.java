package ru.ace.rssreader.async.bitmap;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Async task to resize bitmap to defined width and height
 */
public class BitmapResizeAsyncTask extends AsyncTask<Void, Void, Bitmap> {
    private static final String TAG = BitmapResizeAsyncTask.class.getSimpleName();

    private Bitmap mOriginalBitmap;
    private int mTargetHeight, mTargetWidth;

    protected BitmapResizeAsyncTask(Bitmap originalBitmap, int targetWidth, int targetHeight) {
        mOriginalBitmap = originalBitmap;
        mTargetWidth = targetWidth;
        mTargetHeight = targetHeight;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            return Bitmap.createScaledBitmap(mOriginalBitmap, mTargetWidth, mTargetHeight, false);

        } catch (Exception e) {
            Log.w(TAG, "Bitmap resizing failed", e);
            return null;
        }
    }
}
