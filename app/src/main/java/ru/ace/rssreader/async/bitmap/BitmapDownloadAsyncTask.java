package ru.ace.rssreader.async.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;

/**
 * Async task to load bitmap by url
 */
public class BitmapDownloadAsyncTask extends AsyncTask<Void, Void, Bitmap> {

    private static final String TAG = BitmapDownloadAsyncTask.class.getSimpleName();

    private String mBitmapUrl;

    protected BitmapDownloadAsyncTask(String bitmapUrl) {
        mBitmapUrl = bitmapUrl;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            URL imageUrl = new URL(mBitmapUrl);
            return BitmapFactory.decodeStream((InputStream) imageUrl.getContent());

        } catch (Exception e) {
            Log.w(TAG, "Bitmap downloading failed", e);
            return null;
        }
    }
}
