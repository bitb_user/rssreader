package ru.ace.rssreader.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Basic view holder class for RecyclerView with onClick processing
 */
abstract class ClickViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ClickViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        if (position != RecyclerView.NO_POSITION) {
            onClick(position);
        }
    }

    abstract void onClick(int position);
}
