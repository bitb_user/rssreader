package ru.ace.rssreader.adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.ace.rssreader.Constants;
import ru.ace.rssreader.R;
import ru.ace.rssreader.async.bitmap.BitmapDownloadAsyncTask;
import ru.ace.rssreader.async.bitmap.BitmapResizeAsyncTask;
import ru.ace.rssreader.listener.IRssBitmapLoadListener;
import ru.ace.rssreader.listener.OnSelectRssItemListener;
import ru.ace.rssreader.model.RssItem;

/**
 * Adapter for rss posts list
 */
public class PostsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<RssItem> mRssItems;
    private IRssBitmapLoadListener mRssBitmapLoadListener;
    private OnSelectRssItemListener mOnSelectRssItemListener;

    public PostsListAdapter(List<RssItem> rssItems,
                            IRssBitmapLoadListener rssBitmapLoadListener,
                            OnSelectRssItemListener onSelectRssItemListener) {
        mRssItems = rssItems;
        mRssBitmapLoadListener = rssBitmapLoadListener;
        mOnSelectRssItemListener = onSelectRssItemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_posts_list, parent, false);
        return new RssItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RssItemViewHolder rssItemViewHolder = (RssItemViewHolder) holder;
        rssItemViewHolder.update(position);
    }

    @Override
    public int getItemCount() {
        return mRssItems.size();
    }

    public void updateData(List<RssItem> rssItems) {
        mRssItems = rssItems;
        notifyDataSetChanged();
    }

    private RssItem getItem(int position) {
        return mRssItems.get(position);
    }

    class RssItemViewHolder extends ClickViewHolder {

        @BindView(R.id.imageView_thumbnail)
        ImageView postThumbnail;
        @BindView(R.id.textView_title)
        TextView postTitle;
        @BindView(R.id.textView_description)
        TextView postDesrciption;

        RssItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void update(int position) {
            RssItem rssItem = mRssItems.get(position);
            postTitle.setText(rssItem.getTitle());
            postDesrciption.setText(rssItem.getDescription());

            if (rssItem.getThumbnail() != null) {
                postThumbnail.setImageBitmap(rssItem.getThumbnail());
            } else {
                postThumbnail.setImageBitmap(null);
                loadOnlineRssBitmap(rssItem);
            }
        }

        @Override
        public void onClick(int position) {
            mOnSelectRssItemListener.onSelectRssItem(getItem(position).getGuid());
        }

        /**
         * Async loading of rss item bitmap
         *
         * @param rssItem processed rss item
         */
        private void loadOnlineRssBitmap(final RssItem rssItem) {
            new BitmapDownloadAsyncTask(rssItem.getBitmapUrl()) {
                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    if (bitmap != null) {
                        resizeRssBitmap(bitmap, rssItem);
                    }
                }
            }.execute();
        }

        /**
         * Async bitmap resizing to defined width and height
         *
         * @param bitmap  original bitmap
         * @param rssItem bitmap holder rss item
         */
        private void resizeRssBitmap(Bitmap bitmap, final RssItem rssItem) {
            new BitmapResizeAsyncTask(bitmap,
                    Constants.RssBitmap.THUMBNAIL_WIDTH,
                    Constants.RssBitmap.THUMBNAIL_HEIGHT) {
                @Override
                protected void onPostExecute(Bitmap resizedBitmap) {
                    rssItem.setThumbnail(resizedBitmap);
                    postThumbnail.setImageBitmap(resizedBitmap);
                    mRssBitmapLoadListener.onRssBitmapLoaded(rssItem);
                }
            }.execute();
        }
    }
}

